/*
 * Own Exception
 */
package com.mycompany.exceptions;

/**
 *
 * @author mfontana
 */
public class CalculatorException extends Exception {

    public CalculatorException(String message) {
        super(message);
    }

}
