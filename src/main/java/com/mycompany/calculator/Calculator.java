/*
 * Class Calculator
 * It allows to perform the operations: 
 * addition, subtraction, multiplication and division
 * with integers.
 */
package com.mycompany.calculator;

import com.mycompany.exceptions.CalculatorException;

/**
 *
 * @author mfontana
 */
public class Calculator {

    public int addiction(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }
    
    public int multiplitacion(int a, int b) {
        return a * b;
    }
    
    public int division(int a, int b) throws CalculatorException {
        if (b == 0) {
            throw new CalculatorException("It isn't possible divide by zero.");
        }
        return a / b;
    }

}
