/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.calculator;

import com.mycompany.exceptions.CalculatorException;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author mfontana
 */
public class CalculatorTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();

    }

    @After
    public void tearDown() {
        calculator = null;

    }

    /**
     * Test of addiction method, of class Calculator.
     */
    @Test
    public void testAddiction() {
        System.out.println("addiction");
        int a = 6;
        int b = 4;
        int expResult = 10;
        int result = calculator.addiction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of addiction method with same number, of class Calculator.
     */
    @Test
    public void testAddictionWithSameNumber() {
        System.out.println("addiction with same number");
        int a = 3;
        int b = 3;
        int expResult = 6;
        int result = calculator.addiction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of addiction method with zero, of class Calculator.
     */
    @Test
    public void testAddictionWithZero() {
        System.out.println("addiction with same zero");
        int a = 2;
        int b = 0;
        int expResult = 2;
        int result = calculator.addiction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of subtraction method, of class Calculator.
     */
    @Test
    public void testSubtraction() {
        System.out.println("subtraction");
        int a = 6;
        int b = 4;
        int expResult = 2;
        int result = calculator.subtraction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of subtraction method with second number greater, of class
     * Calculator.
     */
    @Test
    public void testSubtractionWithSecondNumberGreater() {
        System.out.println("subtraction with second number greater");
        int a = 2;
        int b = 8;
        int expResult = -6;
        int result = calculator.subtraction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of subtraction method with zero, of class Calculator.
     */
    @Test
    public void testSubtractionWithZero() {
        System.out.println("subtraction with zero");
        int a = 2;
        int b = 0;
        int expResult = 2;
        int result = calculator.subtraction(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of multiplitacion method, of class Calculator.
     */
    @Test
    public void testMultiplitacion() {
        System.out.println("multiplitacion");
        int a = 4;
        int b = 6;
        int expResult = 24;
        int result = calculator.multiplitacion(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of multiplitacion method by zero, of class Calculator.
     */
    @Test
    public void testMultiplitacionByZero() {
        System.out.println("multiplitacion by zero");
        int a = 4;
        int b = 0;
        int expResult = 0;
        int result = calculator.multiplitacion(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of multiplitacion method by one, of class Calculator.
     */
    @Test
    public void testMultiplitacionByOne() {
        System.out.println("multiplitacion by zero");
        int a = 4;
        int b = 1;
        int expResult = 4;
        int result = calculator.multiplitacion(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of division method, of class Calculator.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        int a = 8;
        int b = 2;
        int expResult = 4;
        try {
            int result = calculator.division(a, b);
            assertEquals(expResult, result);
        } catch (CalculatorException ex) {
            fail("Unexpected Exception ocurred");
        }
    }
    
     /**
     * Test of division method by one, of class Calculator.
     */
    @Test
    public void testDivisionByOne() {
        System.out.println("division");
        int a = 8;
        int b = 1;
        int expResult = 8;
        try {
            int result = calculator.division(a, b);
            assertEquals(expResult, result);
        } catch (CalculatorException ex) {
            fail("Unexpected Exception ocurred");
        }
    }
    
     /**
     * Test of division method by zero, of class Calculator.
     */
    @Test
    public void testDivisionByZero() {
        System.out.println("division");
        int a = 8;
        int b = 0;
        try {
            int result = calculator.division(a, b);
            fail("Expected an CalculatorException to be thrown");
        } catch (CalculatorException ex) {
            assertThat(ex.getMessage(), is("It isn't possible divide by zero."));
        }
    }

}
